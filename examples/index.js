
import _ from 'lodash'
import {format, parse} from 'url'
import Bills from './services/Bills'
import {writeFileSync} from 'fs'

var targetSite = {
  protocol: 'https:',
  host: 'chicago.legistar.com'
};

var paths = {
  bills: '/Legislation.aspx',
  meetings: '/Meetings.aspx',
  legislators: '/Legislators.aspx'
}

var endpoints = {
  bills: format(Object.assign({}, targetSite, { pathname: paths.bills }))
}

var bills = new Bills(endpoints.bills)

function writeJson(json) {
  // write the entries as they come through
  writeFileSync('./results.json', JSON.stringify(bills._data))
}

function next() {
  return bills.next().then(next, writeJson)
}

bills.search().then(next).then(writeJson)
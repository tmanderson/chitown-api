import _ from 'lodash'

const FIELD_MAP = {
  query: 'ctl00$ContentPlaceHolder1$txtText',
  q: 'ctl00$ContentPlaceHolder1$txtText',
  link: '__EVENTTARGET'
}

export default class FormData {
  __VIEWSTATE = ''
  __EVENTTARGET = ''
  __EVENTARGUMENT = ''
  ctl00$ContentPlaceHolder1$txtText = ''
  ctl00_ContentPlaceHolder1_lstMax_ClientState = '{"value":"1000000"}'
  ctl00_ContentPlaceHolder1_lstYearsAdvanced_ClientState = '{"value":"All"}'
  ctl00$ContentPlaceHolder1$btnSearch = 'Search Legislation'

  ctl00$ContentPlaceHolder1$lstYearsAdvanced = '2016'
  ctl00_ContentPlaceHolder1_lstYearsAdvanced_ClientState = '{"logEntries":[],"value":"2016","text":"2016","enabled":true,"checkedIndices":[],"checkedItemsTextOverflows":false}'

  set(key, val) {
    Object.assign(this, { [FIELD_MAP[key] || key]: val })
  }

  get() {
    let output = {};
    for(var i in this) if(output !== undefined) output[i] = this[i];
    return output
  }
}
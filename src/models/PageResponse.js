import _ from 'lodash';
import dom from 'cheerio'

export default class PageResponse {
  current_page = null
  total_pages = null

  constructor(html) {
    this.$ = dom.load(html);
    this.current_page = parseInt(this.currentPage().first().text()) + 1
    this.total_pages = Math.ceil(parseInt(this.totalItems().first().text()) / this.rows().length)
  }

  fields() {
    // $('input[name="__EVENTARGUMENT'] = None
    return Object.assign({},
      ...this.$('input[name="__VIEWSTATE"], input[name="__EVENTVALIDATION"]', 'form')
        .map((i, el) => {
          let $el = this.$(el)
          return { [$el.attr('name')]: $el.attr('value') }
        }).get()
      );
  }

  currentPage(el) {
    return this.$('td.rgPagerCell.NumericPages a.rgCurrentPage', '.rgMasterTable')
  }

  totalItems(el) {
    return this.$('.rmItem.rmFirst a', '#ctl00_ContentPlaceHolder1_menuMain')
  }

  columnNames() {
    return this.$('.rgHeader a', '.rgMasterTable')
      .map( (i, th) => _.trim( _.snakeCase( this.$(th).text() ) ) ).get()
  }

  rows() {
    return this.$('.rgRow, .rgAltRow', '.rgMasterTable')
  }

  pages() {
    return this.$('td.rgPagerCell.NumericPages a', '.rgMasterTable')
  }

  next() {
    return this.currentPage().next()
  }

  prev() {
    return this.currentPage().prev()
  }
}
import _ from 'lodash'

const COLUMNS = [
  'Link',
  'Record #',
  'Type',
  'Status',
  'Introduced',
  'Final Action',
  'Sponsors Ward',
  'Title'
]

export default class Bill {
  type = null
  title = null
  record = null
  status = null
  introduced = null
  final_action = null
  sponsors_ward = null
  link = null

  constructor(properties) {
    Object.assign(this, properties)
    // cols.forEach( (val, i) => _.set(this, _.snakeCase(COLUMNS[i]), _.trim(val)) )
    console.log(this);
  }
}
import _ from 'lodash';
import URL from 'url'
import Bill from '../models/bill'
import PageRequest from './PageRequest'
import { jar } from 'request'

export default class Bills {
  session = null
  _data = []

  constructor(url) {
    this.url = URL.parse(url)

    this.request = new PageRequest({
      jar: jar(),
      url: url
    })
  }

  search(q) {
    return this.request.submit({ q })
      .then((response) => this.add(response), console.log)
  }

  next() {
    var res = this.request.response;

    if(res.current_page >= res.total_pages) {
      return new Promise( (resolve, reject) => {
        reject('There are no more pages')
      })
    }

    console.log(`Getting page ${res.current_page+1}`)

    return this.request.submit({
      link: res.next().first().attr('href').split("'")[1],
      'ctl00$ContentPlaceHolder1$btnSearch': undefined
    })
    .then((response) => this.add(response), console.log)
  }

  prev() {
    var res = this.request.response;

    if(res.current_page < 2 ) {
      return new Promise( (resolve, reject) => {
        reject('There are no previous pages')
      })
    }

    console.log(`Getting page ${res.current_page+1}`)

    return this.request.submit({
      link: res.prev().first().attr('href').split("'")[1],
      'ctl00$ContentPlaceHolder1$btnSearch': undefined
    })
    .then((response) => this.add(response), console.log)
  }

  add() {
    let response = this.request.response;
    let col = response.columnNames()
    let sliceAt = this._data.length
    let $ = response.$

    this._data.push(
      ...response.rows().map( (i, el) => {
        let $cols = $(el).find('td')
        let vals = $cols.map( (i, td) => $(td).text() ).get()

        return new Bill(
          vals.reduce(
            (model, val, i) => Object.assign(model, { [col[i]]: val }),
            { link: $cols.first().find('a').attr('href') }
          )
        );

      }).get()
    );

    return this._data.slice(sliceAt)
  }
}
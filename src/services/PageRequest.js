import _ from 'lodash'
import request from 'request'
import FormData from '../models/FormData'
import { writeFileSync as writeSync, readFile } from 'fs'
import path from 'path'
import PageResponse from '../models/PageResponse'

export default class PageRequest {
  url = ''
  jar = null
  method = 'GET'
  formData = null

  constructor(config = {}) {
    Object.assign(this, config)
    this.formData = new FormData()
    this.init = this._request(config);
  }

  _request(config = {}) {
    console.log(`[${config.method || this.method}] ${config.url || this.url}`);

    config = _.defaults(config, {
      url: this.url,
      method: this.method,
      headers: this.headers,
      jar: this.jar,
      gzip: true
    })

    return new Promise( (resolve, reject) => {
      request(config, (err, res, body) => {
        if(res) console.log(`[${res.statusCode}] ${this.url}`);
        if(err) return reject(err)

        this.response = new PageResponse(body)

        let fields = this.response.fields()
        _.each(fields, (val, key) => this.formData.set(key, val));

        resolve(this.response)
      });
    })
  }

  submit(data) {
    return this.init.then(() => {
      _.each(data, (val, key) => this.formData.set(key, val));
      return this._request({ method: 'POST', form: this.formData.get() })
    })
  }
}